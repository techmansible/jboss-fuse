# Dotnet framework 4.7.1 installation

$dotnet = Get-ItemProperty 'HKLM:SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full' -ErrorAction 'SilentlyContinue' | Select-Object -ExpandProperty "Version"

If ($dotnet -ge "4.7.02558")

{

write-host "Dotnet Framework 4.7.1 or latest version found"
}
Else
{
$ScriptDir = Split-Path $script:MyInvocation.MyCommand.Path
Start-Process -FilePath "$ScriptDir\NDP471-KB4033342-x86-x64-AllOS-ENU.exe" -ArgumentList ('/q', '/norestart') -Wait
}